package com.holiash.model.serializeable;

public class Country {
  private String name;
  private long population;

  public Country(String name, long population) {
    this.name = name;
    this.population = population;
  }

  @Override
  public String toString() {
    return "Country{" +
      "name='" + name + '\'' +
      ", population=" + population +
      '}';
  }
}
