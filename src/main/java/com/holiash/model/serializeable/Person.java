package com.holiash.model.serializeable;

import java.io.Serializable;

public class Person implements Serializable {

  private int age;
  private String name;
  private transient Country country;

  public Person(int age, String name, Country country) {
    this.age = age;
    this.name = name;
    this.country = country;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  @Override
  public String toString() {
    return "Person{" +
      "age=" + age +
      ", name='" + name + '\'' +
      ", country=" + country +
      '}';
  }
}
