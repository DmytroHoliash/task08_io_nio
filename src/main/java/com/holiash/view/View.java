package com.holiash.view;

public interface View {

  void show();
}
