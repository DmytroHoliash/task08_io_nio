package com.holiash.view;

public interface Command {

  void execute();
}
