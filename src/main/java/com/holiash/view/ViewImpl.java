package com.holiash.view;

import com.holiash.controller.Controller;
import com.holiash.util.Menu;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewImpl implements View {

  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  private Controller controller;
  private List<String> menu;
  private Map<Integer, Command> menuMethods;

  public ViewImpl(Controller controller) {
    this.controller = controller;
    menu = new ArrayList<>();
    menuMethods = new HashMap<>();
    menu.add("1 - serialize object");
    menu.add("2 - test reading using usual reader");
    menu.add("3 - test reading using buffered readers");
    menu.add("4 - find comments in java code");
    menu.add("5 - show content of directory");
    menu.add("0 - exit");
    menuMethods.put(1, this::serialize);
    menuMethods.put(2, this::testUsualReading);
    menuMethods.put(3, this::testBufferedReading);
    menuMethods.put(4, this::findComments);
    menuMethods.put(5, this::showDirectory);
  }

  private void serialize() {
    controller.serializeExamples();
  }

  private void testUsualReading() {
    controller.testReadingUsual();
  }

  private void testBufferedReading() {
    controller.testReadingBuffered();
  }

  private void findComments() {
    System.out.println("Input file name: ");
    try {
      String fileName = br.readLine();
      controller.findComments(fileName);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private void showDirectory() {
    System.out.println("Input directory or file name: ");
    try {
      String fileName = br.readLine();
      controller.showContentOfDirectory(fileName);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  public void show() {
    Menu.run(this.menu, this.menuMethods);
  }
}
