package com.holiash.util;

import com.holiash.view.Command;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

public class Menu {

  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  private static void showMenu(List<String> menu) {
    menu.forEach(System.out::println);
  }

  public static void run(List<String> menu, Map<Integer, Command> menuMethods) {
    int choice = 0;
    do {
      showMenu(menu);
      System.out.println("Make your choice: ");
      try {
        choice = Integer.parseInt(br.readLine());
        if (menuMethods.containsKey(choice)) {
          menuMethods.get(choice).execute();
        }
      } catch (IOException | NumberFormatException e) {
        System.out.println(e.getMessage());
      }
    } while (choice != 0);
  }
}
