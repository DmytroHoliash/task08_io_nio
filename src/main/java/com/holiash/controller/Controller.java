package com.holiash.controller;

import com.holiash.model.serializeable.Country;
import com.holiash.model.serializeable.Person;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {

  private static final Logger LOGGER = LogManager.getLogger(Controller.class);

  public void serializeExamples() {
    Person person = new Person(15, "Roman", new Country("India", 150000000));
    LOGGER.info("Person before serialization: " + person.toString());
    try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("person.ser"))) {
      out.writeObject(person);
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
    Person deserialized = null;
    try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("person.ser"))) {
      deserialized = (Person) in.readObject();
      LOGGER.info("Person after deserialization " + deserialized.toString());
    } catch (ClassNotFoundException | IOException e) {
      LOGGER.error(e.getMessage());
    }
  }

  public void testReadingUsual() {
    long start;
    long finish;
    try (InputStream in = new FileInputStream("50mb.txt")) {
      LOGGER.info("Reading 50mb file using usual reader");
      start = System.nanoTime();
      int data = in.read();
      while (data != -1) {
        data = in.read();
      }
      finish = System.nanoTime();
      LOGGER.info("Time = " + (finish - start));
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
  }

  public void testReadingBuffered() {
    long start;
    long finish;
    try (BufferedInputStream in = new BufferedInputStream(new FileInputStream("50mb.txt"))) {
      LOGGER.info("Reading 50mb file using buffered reader: ");
      start = System.nanoTime();
      int data = in.read();
      while (data != -1) {
        data = in.read();
      }
      finish = System.nanoTime();
      LOGGER.info("Time = " + (finish - start));
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
    int size = 1024;
    sizedBufferReading(size);
    size = 1024 * 1024;
    sizedBufferReading(size);
    size = 10 * 1024 * 1024;
    sizedBufferReading(size);
  }

  private void sizedBufferReading(int size) {
    long start;
    long finish;
    try (BufferedInputStream in = new BufferedInputStream(new FileInputStream("50mb.txt"), size)) {
      LOGGER.info("Reading 50mb file using buffered reader with size: " + size);
      start = System.nanoTime();
      int data = in.read();
      while (data != -1) {
        data = in.read();
      }
      finish = System.nanoTime();
      LOGGER.info("Time = " + (finish - start));
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
  }

  public void findComments(String fileName) {
    try (BufferedReader in = new BufferedReader(new FileReader(fileName))) {
      List<String> comments = new ArrayList<>();
      StringBuilder comment = new StringBuilder();
      int data = in.read();
      while (data != -1) {
        char curr = (char) data;
        if (curr == '/') {
          comment.append(curr);
          data = in.read();
          curr = (char) data;
          comment.append(curr);
          if (curr == '/') {
            while (curr != '\r') {
              data = in.read();
              curr = (char) data;
              comment.append(curr);
            }
          } else if (curr == '*') {
            while (curr != '/') {
              data = in.read();
              curr = (char) data;
              comment.append(curr);
            }
          }
          comments.add(comment.toString());
          comment = new StringBuilder();
        }
        data = in.read();
      }
      comments.forEach(System.out::println);
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }

  }

  public void showContentOfDirectory(String directory) {
    File file = new File(directory);
    if (file.exists()) {
      showDirectory(file, "");
    } else {
      LOGGER.info("No such file or directory");
    }
  }

  private void showDirectory(File file, String s) {
    LOGGER.info(s + "Directory: " + file.getName());
    s = s + "  ";
    File[] files = file.listFiles();
    for (File f : files) {
      if (f.isDirectory()) {
        showDirectory(f, s);
      } else {
        LOGGER.info(s + f.getName());
      }
    }
  }

}
